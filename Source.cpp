#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	
	cout << "Enter the amount of numbers to sort: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int randNum = rand() % 101;

		unordered.push(randNum);
		ordered.push(randNum);
	}
	system("cls");

	while (true)
	{
		cout << "Unordered Numbers: " << endl;
		for (int i = 0; i < unordered.getSize(); i++)
		{
			cout << unordered[i] << "   ";
		}
		cout << endl;

		cout << "\nOrdered Numbers: " << endl;
		{
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
		}
		cout << "\n" << endl;

		cout << R"(		What would you like to do?
		[1] Remove an element
		[2] Search for an element
		[3] Expand and generate random values)" << endl;

		int input;
		int choice;
		cin >> choice;
		cout << endl;

		if (choice != 1 && choice != 2 && choice != 3)
		{
			cout << "Please try again \n";
			cin >> choice;
		}
		else if (choice == 1)
		{
			cout << "Enter the index of the number you want to remove: \n";
			cin >> input;

			unordered.remove(input);
			unordered.pop();

			ordered.remove(input);
			ordered.pop();
		}
		else if (choice == 2)
		{
			cout << "What number do you want to search for? \n";
			cin >> input;
			cout << endl;

			cout << "UNORDERED ARRAY (Linear Search):\n";
			int result = unordered.linearSearch(input);

			if (result >= 0)
			{
				cout << "- " << input << " was found at index " << result << ". \n";
			}
			else
			{
				cout << "- " << input << " not found." << endl;
			}
			cout << endl;

			cout << "ORDERED ARRAY (Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
			{
				cout << "- " << input << " was found at index " << result << ".\n";
			}
			else
			{
				cout << "- " << input << " not found." << endl;
			}
			cout << endl;


		}
		else if (choice == 3)
		{
			cout << "Input size of expansion: \n";
			cin >> input;

			for (int i = 0; i < input; i++)
			{
				int randNum = rand() % 101;
				unordered.push(randNum);
				ordered.push(randNum);
			}
		}

		system("pause");
		system("cls");
	}
}